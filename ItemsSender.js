/**
 * Send items to server
 */
class ItemsSender {

	/**
	 * Create sender object
	 * @param serverUrl - url for requests
	 */
	constructor(serverUrl) {
		this.serverUrl = serverUrl;
	}

	/**
	 * Send items on url passed in constructor
	 * @param items - array of items objects
	 * @param itemsKey - key word to describe this items collection
	 * @param onLoad {function} - function, called for server response
	 */
	send(items, itemsKey, onLoad) {
		console.debug(`ItemsSender: try to send items(${items.length}) on server.`);

		let data =
			"itemsJSON=" + encodeURIComponent(JSON.stringify(items)) + "&" +
			"key=" + encodeURIComponent(itemsKey);

		GM_xmlhttpRequest({
			method: "POST",
			url: this.serverUrl,
			data: data,
			headers: {"Content-Type": "application/x-www-form-urlencoded"},
			onload: response => {
				let responseObj = JSON.parse(response.responseText);
				console.debug(`ItemsSender: server response contains items(${responseObj.length}).`);

				if (onLoad)
					onLoad(responseObj);
			},
			onerror: () => console.debug(`Cant send items (key=${itemsKey})`)
		});
	}
}